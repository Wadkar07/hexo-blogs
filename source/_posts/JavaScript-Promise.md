---
title: JavaScript Promise
date: 2023-03-14 13:13:18
tags:
---
## **Promises.all**
___
`Promise.all` is a method which allows you to execute multiple promises simultaneously and wait for all of them to complete before moving on to the next step in your code.

It takes an array of promises as input and returns a new promise that resolves with an array of the resolved values of the input promises, in the same order as the original array.

If any of the input promises reject, then the resulting promise returned by `Promise.all` will also reject, with the reason of the first promise that rejected. Here's an example that shows how it works

```JS
const fs = require('fs');
const promises = [
    new Promise((resolve, reject) => {
        fs.readFile('file.txt','utf-8',(err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve('done 1');
            }
        });
    }),
    new Promise((resolve, reject) => {
        fs.readFile('file.txt','utf-8',(err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve('done 2');
            }
        });
    }),
    new Promise((resolve, reject) => {
        fs.readFile('file.txt','utf-8',(err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve('done 3');
            }
        });
    })
];

Promise.all(promises)
    .then(results => {
        console.log(results); 
    })
    .catch(error => {
        console.error(error); 
    });
```
<div align= center> 

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-14-12-59-26.png)

</div>

> *In simpler words, `Promise.all` lets you run many promises at once and wait for them all to finish, so you can do something else once they're all done.* 


## **Promise.allSettled**
___
Promise.allSettled is a method in JavaScript that allows you to execute multiple promises simultaneously and wait for all of them to settle (either resolve or reject) before moving on to the next step in your code.

It takes an array of promises as input and returns a new promise that resolves with an array of objects representing the state of each promise in the input array. Each object contains either a status property set to "fulfilled" or "rejected", and a value or reason property that contains the resolved value or reject reason, respectively. Here's an example that shows how it works
```JS
const fs = require('fs');
const promises = [
    new Promise((resolve, reject) => {
        fs.readFile('file.txt','utf-8',(err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve('done 1');
            }
        });
    }),
    new Promise((resolve, reject) => {
        fs.readFile('file.tx','utf-8',(err,data)=>{
            if(err){
                reject(err.message);
            }
            else{
                resolve('done 2');
            }
        });
    }),
    new Promise((resolve, reject) => {
        fs.readFile('file.txt','utf-8',(err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve('done 3');
            }
        });
    })
];

Promise.allSettled(promises)
    .then(results => {
        console.log(results); 
    })
    .catch(error => {
        console.error(error); 
    });
```
<div align= center> 

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-14-12-59-57.png)

</div>

> *In simpler words, Promise.allSettled lets you run many promises at once and wait for them all to finish (either successfully or unsuccessfully), so you can do something else once they've all settled. This is useful when you want to handle all the promises' outcomes, regardless of whether they were successful or not.*

## **Promise.any**
Promise.any is a method in JavaScript that allows you to execute multiple promises simultaneously and wait for the first one to successfully fulfill (resolve) before moving on to the next step in your code.

It takes an array of promises as input and returns a new promise that resolves with the value of the first promise in the input array to successfully fulfill. If all of the promises in the input array reject, the resulting promise returned by Promise.any will reject with the reason of the last promise that rejected.

```JS
const promises = [
    new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('first');
        }, 1000)
    }),
    new Promise((resolve, reject) => {
        setTimeout(() => {
            reject(new Error('second'));
        }, 500)
    }),
    new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('third')
        }, 2000)
    })
];

Promise.any(promises)
    .then(result => {
        console.log(result);
    })
    .catch(error => {
        console.error(error.message);
    });
```
<div align= center> 

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-14-13-00-23.png)

</div>

> *In simpler words, Promise.any lets you run many promises at once and wait for the first one to successfully fulfill, so you can use that result in your code. This is useful when you don't need to wait for all the promises to finish and only care about the first successful result.*

## **Promise.race**
Promise.race is a method in JavaScript that allows you to execute multiple promises simultaneously and wait for the first one to settle (either resolve or reject) before moving on to the next step in your code.

It takes an array of promises as input and returns a new promise that resolves with the value of the first promise in the input array to settle (either resolve or reject). If the first promise to settle rejects, the resulting promise returned by Promise.race will also reject with the same reason.
```JS
const promises = [
    new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('first');
        }, 1000)
    }),
    new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('second');
        }, 500)
    }),
    new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('third')
        }, 2000)
    })
];

Promise.race(promises)
    .then(result => {
        console.log(result);
    })
    .catch(error => {
        console.error(error.message);
    });
```
<div align= center> 

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-14-13-00-45.png)
</div>

> *In simpler words, Promise.race lets you run many promises at once and wait for the first one to settle, so you can use that result in your code. This is useful when you don't need to wait for all the promises to finish and only care about the first settled result, whether it is a success or failure.*


## **Summary**
+ `Promise.all` : Runs multiple promises in parallel and waits for all of them to fulfill before moving on to the next step in your code.

+ `Promise.allSettled` : Runs multiple promises in parallel and waits for all of them to settle (either resolve or reject) before moving on to the next step in your code.

+ `Promise.any` : Runs multiple promises in parallel and waits for the first one to fulfill before moving on to the next step in your code.

+ `Promise.race` : Runs multiple promises in parallel and waits for the first one to settle (either resolve or reject) before moving on to the next step in your code.

Each of these methods is useful in different scenarios, depending on what you want to achieve with your asynchronous code. They all help simplify the management of asynchronous operations and can make your code more efficient and concise.
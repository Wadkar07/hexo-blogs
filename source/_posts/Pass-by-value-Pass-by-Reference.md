---
title: Pass by value & Pass by Reference
date: 2023-03-02 11:56:03
tags:
---
 ***In JavaScript, all function arguments are always passed by value.** It means that JavaScript copies the values of the variables into the function arguments.*
 
  ***Any changes that you make to the arguments inside the function do not reflect the passing variables outside of the function.** In other words, the changes made to the arguments are not reflected outside of the function.*
  
  *If function arguments are passed by reference, the changes of variables that you pass into the function will be reflected outside the function. This is not possible in JavaScript.*

# Pass-by-value

Lets take a simple example of square of a number

```js
function squareOfNumber (number) {
    number = number * number;
    return number;
}

let mainVariable = 10;
let square = squareOfNumber(mainVariable);

console.log(square);
console.log(mainVariable);
```
<div align="center">

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-11-44-02.png)

</div>

First define a function ``` squareOfNumber() ``` that accepts a number as an argument and function returns the square of that number.

After that declare a variable ```mainNumber ``` and initialize it

<div align="center">

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-12-12-34.png)

</div>

Then, pass the ```mainNumber``` variable into the ```squareOfNumber()``` function. When passing the variable ```mainNumber``` to the ```squareOfNumber()``` function, JavaScript copies ```mainNumber``` value to the ```number``` variable.
<div align="center">

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-12-22-13.png)

</div>

After that, **the `squareOfNumber()` function changes the `number` variable**. However, *it does not impact the value of the `mainVariable` variable because `number` and `mainVariable` are separate variables.*

<div align="center">

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-12-28-27.png)

</div>

Finally, the value of the `mainNumber` variable does not change after the `squareOfNumber()` function completes.

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-12-33-51.png)

</div>

If JavaScript used the pass-by-reference, the variable `mainVariable` would change to 100 after calling the function.

# Pass-by-value

It’s not obvious to see that reference values are also passed by values. For example:

```js
let person = {
    name : 'Shubham',
    age : 23,
};

function incrementAge(object){
    object.age += 1;
}

incrementAge(person);
console.log(person);
```

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-12-44-55.png)

</div>
First, define the person variable that references an object with two properties name and age:

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-12-55-51.png)

</div>

Next, define the `incrementAge()` function that accepts an object `object` and increases the `age` property of the `object` argument by one.

Then, pass the person object to the `incrementAge()` function:

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-13-01-32.png)
</div>

**Internally, the JavaScript engine creates the `object` reference and make this variable reference the same object that the `person` variable references.**

After that, increment the age property by one inside the `incrementAge()` function via the `object` variable

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-13-06-01.png)

</div>

Finally, accessing the object via the `person` reference:

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-13-08-48.png)

</div>

It seems that JavaScript passes an object by reference because the change to the object is reflected outside the function. However, this is not the case.

In fact, **when passing an object to a function, you are passing the reference of that object, not the actual object**. Therefore, the function can modify the properties of the object via its reference.

However, you cannot change the reference passed into the function. For example:

```js
let person = {
    name : 'Shubham',
    age : 23,
};

function incrementAge(object){
    object.age += 1;
    object = { name: 'Shubham Wadkar', age: 22 };
}

incrementAge(person);
console.log(person);
```
output : 

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-13-12-36.png)

</div>

In this example, the `incrementAage()` function changes the age property via the `object` argument:

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-13-06-01.png)

</div>

and makes the `object` reference another object:

<div align='center'>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-02-13-24-00.png)

</div>

However, **the `person` reference still refers to the original object whose the `age` property changes to `24`. In other words, the `incrementAge()` function doesn’t change the `person`** reference.

# Summary
+ **In JavaScript, all function arguments are always passed by value.**
+ **JavaScript passes all arguments to a function by values.**
+ **Function arguments are local variables in JavaScript.**
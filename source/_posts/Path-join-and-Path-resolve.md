---
title: Path.join and Path.resolve
date: 2023-03-15 16:01:25
tags: 
---

Node.js has a built-in path module, which can be used for creating file and directory paths in our application. Two methods of this module are which serve the similar purpose of creating paths. 

+ **`join()`**
+ **`resolve()`**


## **Join**

The **`path.join`** is a method in Node.js that is used to join multiple path segments together using the platform-specific separator as a delimiter, then normalizes the resulting path.  On Windows platform, the separator is \ (backslash), whereas on Unix-based platforms like Linux and macOS, the separator is / (forward slash).The syntax for path.join is as follows:

#### **Join Syntax**

```JS
const path = require('path');

path.join(path1,path2,path3,pathN);
```

#### **Join Examples** 


```JS
    const path = require('path');

    const path1 = 'parent';
    const path2 = 'child';
    const path3 = 'grand child';

    const fullPath = path.join(path1,path2,path3);

    console.log(fullPath);
```

<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-15-16-12-27.png)
</div>

What it actually does is join all the paths in order that is **`path`, `path2`** and **`path3`** which results **`parent/child/grandChild`** with a delimeter **`/`** and if absolute path is required we can use **`__dirname`** as shown in following example

```JS
const path = require("path");

const path1 = "/parent";
const path2 = "/child";
const path3 = "/grandChild";

const fullPath = path.join(path1, path2, path3);

console.log(fullPath);
```

<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-15-16-22-24.png)
</div>

## **Resolve**

The **`path.resolve`**  is a method in Node.js that is used to resolve a sequence of path segments into an absolute path. It works by taking one or more path segments as input and resolving them into an absolute path, based on the current working directory of the Node.js process.

The syntax for path.resolve is as follows:

#### **Resolve Syntax**

```JS
const path = require('path');

path.resolve(path1,path2,path3,pathN);
```

Here, path1, path2, path3 that are needed to be resolved. If multiple path segments are provided, they are processed from right to left, with each subsequent path segment being resolved relative to the previous one.

#### **Resolve examples**

```JS
    const path = require("path");

    const path1 = "/parent";
    const path2 = "/child";
    const path3 = "/grandChild";

    const fullPath = path.resolve(path1, path2, path3);

    console.log(fullPath);
```
<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-15-16-42-36.png)
</div>

> What about the **`path1`** and **`path2`**?
> Why it is only showing the **`path3`**? 

let see how it works first. It read the path from right to left and don't stop until it finds **`\`** and once it finds **`/`** it make it the root directory. Let's check it step by step. In following example:

```JS
const path = require("path");

const path1 = "/parent";
const path2 = "/child";
const path3 = "grandChild";

const fullPath = path.resolve(path1, path2, path3);

console.log(fullPath);
```
<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-15-16-45-27.png)
</div>

If we start reading from right to left the fisrt **`/`** is at **`path2`** so **`resolve`** made **`path2`** that is **`child`** root directory.

>what if we don't provide any with **`/`**?
>What will be the output/path will be resolved?

 lets check it in following example.

```JS
    const path = require("path");

    const path1 = "parent";
    const path2 = "child";
    const path3 = "grandChild";

    const fullPath = path.resolve(path1, path2, path3);

    console.log(fullPath);
```
<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-15-16-53-34.png)
</div>

What path is this where it came from and only this not any other. Lets defragment this path output. As we know till now that it reads path from right to left,so it startedtaking every parent paent directory until it found **`/`**, which is root directory and it resolved the path very root directory of the system. Which is also the absolute path.

## Summary

+ path.join is used to join multiple path segments together using the platform-specific separator as a delimiter, and then normalizes the resulting path. It automatically resolves any relative path segments (current and parent directory) and removes redundant separators.

+ path.resolve is used to resolve a sequence of path segments into an absolute path, based on the current working directory of the Node.js process. It also resolves any relative path segments (working and parent directory) and always returns an absolute path, regardless of whether the input path segments are absolute or relative.

+ path.join is used for combining path segments into a single path, while path.resolve is used for resolving a sequence of path segments into an absolute path.
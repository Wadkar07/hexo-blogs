---
title: Sorting in JavaScript
date: 2023-03-06 13:30:41
tags:
---
## **Strings**
Sort String in JavaScript is used to sort the strings according to alphabetical order i.e it converts the given elements into strings by comparing the UTF-16 code sequences into unit values. In JavaScript, we use sort() method to sort the strings and return the sorted array of strings. Default order being ascending order, built upon conversion for elements into strings. 

***Syntax:***

As said, the sort() method sorts elements with the smallest value at the start and largest at last.
```JS
<array_name>.sort();
```
Let the candidateName be an array of strings,

```JS
candidateName.sort();
```
There is no need for arguments to be passed to the sort() method.
sort() can have an optional parameter as compareFunction used to define customized sorting order if any.

+ sort() method returns array after sorting the elements of the array.
+ We have two ways to sort strings such as Using sort() method or  Using Loop.
+ On using sort() method, we are using a predefined method by JavaScript to sort array of strings.
+ Sort() method is applicable only for alphabetic strings.
+ ***Sort() method does not support array of numbers.***
+ There is one simple approach in sorting the array of elements i.e. Looping an array and comparing each element to put the string elements at its position.
+ Here, Looping can be applied to numbers in an array.

### **Example**
Simple Sort string in JavaScript using sort() method
```JS
let candidateName = ['Sandeep', 'Vishrut', 'Sumen', 'Bhabesh', 'Sunil','Ranjeet'];

candidateName.sort();
console.log(candidateName);
```
<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-06-14-01-02.png)
</div>

## **Different case strings**
We have witnessed how sort functions works in string, but does case of string matter while sorting, let's check it out for real.
```JS
let candidateName = ['Sandeep', 'Vishrut', 'Sumen', 'bhabesh', 'Sunil','Ranjeet'];

candidateName.sort();
console.log(candidateName);
```
<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-06-14-12-07.png)
</div>

the lowercase string is sorted last and uppercase string get stored first, but this is not what the desired output is.For that we have to use different approach, we will use `higher order function` and convert all string to lowercase and then compare them.
```JS
let candidateName = ['Sandeep', 'Vishrut', 'Sumen', 'bhabesh', 'Sunil','Ranjeet'];

candidateName.sort(function(nameA,nameB) {
    nameA = nameA.toLowerCase();
    nameB = nameB.toLowerCase();
    if (nameA == nameB){
        return 0;
    } 
    if (nameA > nameB){
        return 1;
    }
    else{
        return -1;
    }
});
console.log(candidateName);
```
<div align= center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-06-14-24-04.png)

</div>

## **Integers**
Sorting integer is same as case sensitive sorting string. Same in the sense is like it also don't support `<arrayName>.sort()` and can be done with higher oder function. 
```JS
let numbersArray = [23,3,54,12,5];

numbersArray.sort(function(numberA,numberB) {
    if (numberA == numberB){
        return 0;
    } 
    if (numberA > numberB){
        return 1;
    }
    else{
        return -1;
    }
});

console.log(numbersArray);
```
<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-06-14-39-50.png)
</div>

## **Floating point numbers**
Floating point numbers can be sorted as same as integers. let check using the following example.

```JS
let floatingNumbersArray = [23.3,3.1,54.8,12.02,5.63];

floatingNumbersArray.sort(function(floatA,floatB) {
    if (floatA == floatB){
        return 0;
    } 
    if (floatA > floatB){
        return 1;
    }
    else{
        return -1;
    }
});
console.log(floatingNumbersArray);
```

<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-06-15-22-59.png)
</div>

## **Objects**
let take a look at the object and see how to sort it. we have the following data of candidate in object, and let sort them based on there age.
```JS
let candidateData = [
    {
        name: "Sandeep",
        age: 26
    },
    {
        name: 'Vishrut',
        age: 22
    },
    {
        name: 'Sumen',
        age: 25,
    },
    {
        name: 'Bhabesh',
        age: 23,
    },
    {
        name: 'Sunil',
        age: 25,
    },
    {
        name: 'Ranjeet',
        age: 23,
    }
];
candidateData.sort((candidateA, candidateB) => {
    if (candidateA.age < candidateB.age) {
        return -1;
    }
    if (candidateA.age > candidateB.age) {
        return 1;
    }
    return 0;
});
```

<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-06-15-36-42.png)
</div>

 here we have sorted based on value of property, what if we have to sort the object based on the keys of the object, but here we have same keys, but what we have multiple keys lets check that in next section.

## **Multiple keys in an object**
We have seen how to sort object with value of property but what if we have object with multiple keys let see an example
```JS
    let candidateData = {
    "Sandeep": {
        city: 'Azamgad',
        age: 26
    },
    'Vishrut': {
        city: 'Shimla',
        age: 22
    },
    'Sumen': {
        city: 'Patna',
        age: 25,
    },
    'Bhabesh': {
        city: 'Jamshedpur',
        age: 23,
    },
    'Sunil': {
        city: 'Bhubaneshwar',
        age: 25,
    },
    'Ranjeet': {
        city: 'Delhi',
        age: 23,
    }
};

const orderedCandidateName = Object.keys(candidateData)
    .sort()
    .reduce((orderedCandidateName, candidateName) => {
        orderedCandidateName[candidateName] = candidateData[candidateName];
        return orderedCandidateName;
    },{});

console.log(orderedCandidateName);
```
<div align=center>

![image](https://www.linkpicture.com/q/Screenshot-from-2023-03-06-16-02-06.png)
</div>

here we have sorted the object with multiple keys using higher order function `reduce`.

## Summary
+ String array can be sorted using `<arrayName>.sort()` method.
+ Integer array can be sorted with hgher order function as it doesn't support `<arrayName>.sort()` method.
+ Floating point number can be sorted as same as integer array.
+ Object can be sorted using higher order function `sort` by value of properties.
+ Object can be sorted based on keys if multiple keys exists, using chained higher ordered function.

---
title: Truthy & Falsy Values
date: 2023-02-22 11:15:33
tags: JavaSript
---
___
 Every value in javascript is truthy value if its not defined as falsy,
 <div align="center">
 
 ![](https://i.gifer.com/1eg.gif )
 </div>

but there are some exceptions. we will cover that ahead in the blog.

 ## Difference between truthy and true values & falsy and false values

Truthy and true might sound similar and same goes for falsy and false but, there is a factual difference in these. the difference might shock you but thats what javascript is for.

## Truthy values vs True values

**If the value isn't literally "true" but evaluates as true, we call it "truthy".**

<div align="center">
    <img src="https://media.tenor.com/_OiXuFhs0xUAAAAC/wtf-confused.gif">
</div>



Wait let me give you some example. In following code we can see the data type of thetwo different value are different but as I mentioned if the value isn't literally "true" but evaluates as true, we call it "truthy."

```js
let boolVariable = true;
let stringVariable = "true";
console.log(typeof boolVariable)
console.log(typeof stringVariable)
```

<div align="center">

![image](https://www.linkpicture.com/q/image1_6.png)
</div>

Here we can see that ```stringValue``` is a string type and ```boolValue``` is a boolean but here the only true value we can seeif ```boolValue```. Now lets check how to respond to the conditional statement, we already know that conditional statement responds only to true values.

```js
let boolVariable = true;
let stringVariable = "true";

if(boolVariable){
    console.log("It is true value");
}
if(stringVariable){
    console.log("It is a truthy value");
}
```
before checking the output given below why don't you try youself to predict the output yourself, I am sure you might guess correct.

<details >
<summary>Output</summary>
Or might not!!
<div align="center">

 ![image](https://www.linkpicture.com/q/Screenshot-from-2023-02-22-12-20-44.png)

 ![](https://media.tenor.com/zE4WEqrRuGUAAAAM/upisnotjump-excuse-me.gif)
 
 </div>
 </details>
 

## Falsy values vs false values

 If the value isn't literally "false" but evaluates as false, we call it "falsey. Yeah just same as truthy, lets see example for this too, [because...](https://www.youtube.com/watch?v=22rCPuPh1Gw)

 ```js
let boolVariable = false;
let stringVariable = 0;
console.log(typeof boolVariable)
console.log(typeof stringVariable)
 ```
<div align="center">

 ![image](https://www.linkpicture.com/q/Screenshot-from-2023-02-22-12-42-12.png)

</div>
 here we can check there type and there value and now lets see how they respond to condtional statement,

 ```js
 let boolVariable = false; // here boolVariable is defined as false
let stringVariable = 0;

if(boolVariable){
    console.log("It is false value");
}
if(stringVariable){
    console.log("It is a falsy value");
}
 ```
<div align="center">

 ![image](https://www.linkpicture.com/q/Screenshot-from-2023-02-22-12-44-56.png)

</div>
## Lets see the exceptipns of falsy

| Values      |  description   |
|-------------|----------------|
| 0           | (zero)         |
| -0          | (minus zero)   |
| 0n          | (BigInt zero)  |
| '', "", ``  | (empty string) |
| null        | NULL           |
| undefined   | undefined      |
| NaN         | Not a Number   |


<div align="center">

 Congratulations you've mastered truth and falsy values
 </br>

 ![](https://media.giphy.com/media/8Iv5lqKwKsZ2g/giphy.gif)
 </br>
</div> 

 ## Conclusion
 + Truthy and true are not same.
 + False and Falsy are not same.
 + If its not defined as false it is true.
 + There are some exceptions to Falsy.